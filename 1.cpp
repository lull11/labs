﻿#include <iostream>
#include <math.h>
using namespace std;
#define EPS 0.000000001

class ComparatorInt {
public:
	bool operator()(int value1, int value2) {
		return value1 == value2;
	}
};

class ComparatorDouble {
public:
	bool operator()(double value1, double value2 ){
		return fabs(value1 - value2) < EPS;
	}
};

template<class T, class Comparator = ComparatorInt>
class List {
public:
	List();
	~List();

	int Insert(T newVal, T firstVal, T SecondVal);
	void Push(const T&);
	
private:
	struct Node {
		T data;
		Node* next;
		Node() :next(nullptr) {};
		Node(const T& data1) {
			data = data1;
		}
	};

	Node* head;
	Comparator cmp;
};

template<class T, class Comparator>
List<T, Comparator>::List() {
	head = nullptr;
}

template<class T, class Comparator>
List<T, Comparator>::~List() {
	while (head != nullptr) {
		Node* current = head;
		head = head->next;
		delete current;
	}
}

template<class T, class Comparator>
void List<T, Comparator>::Push(const T& data){
	Node* tmp = head;
	head = new Node(data);
	head->next = tmp;
}

template<class T, class Comparator>
int List<T, Comparator>::Insert(T newVal, T firstVal, T SecondVal) {
	if (head == nullptr) {
		return 0;
	}
	Node* search = head;
	int countIns = 0;
	while (search->next != nullptr) {
		
		if (cmp(search->data, firstVal) &&
			cmp(search->next->data, SecondVal)) {
			Node* newNode = new Node(newVal);
			newNode->next = search->next;
			search->next = newNode;
			countIns++;

		}
		search = search->next;
	}
	return countIns;
}



int main() {
<<<<<<< Updated upstream
	List a;
	a.Push(1.0);
	a.Push(2.0);	
	a.Push(1.0);	
	a.Push(2.0);	
	a.Push(1.0);	
	a.Push(2.0);	
	a.Push(7.0);
	cout<< a.Insert(5.0, 2.0, 1.0)<< endl;
=======
	List<int> a;
	a.Push(1);
	a.Push(2);	
	a.Push(1);	
	a.Push(2);	
	a.Push(1);	
	a.Push(2);	
	a.Push(7);
	cout<< a.Insert(2, 2, 1)<< endl;
>>>>>>> Stashed changes
	return 0;
}